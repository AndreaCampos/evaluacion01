<%-- 
    Document   : resultado
    Created on : 5 may 2021, 22:49:49
    Author     : AndreaCampos
--%>

<%@page import="cl.model.CalculaInteres"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
        CalculaInteres calculo = (CalculaInteres) request.getAttribute("CalculaInteres");
    
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
          <h1>El resultado del interés es: <%=calculo.getCalculo()%>  </h1>
    </body>
</html>
